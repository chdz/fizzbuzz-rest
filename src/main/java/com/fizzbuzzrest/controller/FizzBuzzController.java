package com.fizzbuzzrest.controller;

import com.fizzbuzzrest.model.FizzBuzz;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
@RequestMapping(value="/fizzbuzz")
public class FizzBuzzController {
    @RequestMapping(value = "/{upper}", method = RequestMethod.GET)
    public FizzBuzz fizzbuzz(@PathVariable String upper) {
        return new FizzBuzz(Long.valueOf(upper));
    };
}



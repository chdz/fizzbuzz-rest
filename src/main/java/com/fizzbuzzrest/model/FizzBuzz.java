package com.fizzbuzzrest.model;

import java.util.ArrayList;

public class FizzBuzz {
    private final int fizz = 3;
    private final int buzz = 5;

    private ArrayList<Integer> fizzArray = new ArrayList<Integer>();
    private ArrayList<Integer> buzzArray = new ArrayList<Integer>();
    private ArrayList<Integer> fizzBuzzArray = new ArrayList<Integer>();

    public FizzBuzz(Long limit) {
        for (int i = 1; i <= limit; i++) {
            if (i % fizz == 0 && i % buzz == 0) {
                this.fizzBuzzArray.add(i);
            } else if (i % fizz == 0) {
                this.fizzArray.add(i);
            } else if (i % buzz == 0) {
                this.buzzArray.add(i);
            }
        }
    }
}
